//
//  MoviesList.h
//  iMovieManager
//
//  Created by HaseeB Arif on 8/9/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddMovie.h"

@protocol MovieDataProtocol;
@protocol mSortProtocol;

@interface MoviesList : UITableViewController<UITableViewDelegate,UITableViewDataSource>

//protected instance variables.
@property NSMutableArray *moviesNames;
@property NSMutableArray *moviesYears;

- (IBAction)sortButton:(id)sender;
- (IBAction)addButton:(id)sender;
- (IBAction)fetchButton:(id)sender;


@end
