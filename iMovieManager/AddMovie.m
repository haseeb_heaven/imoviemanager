//
//  AddMovie.m
//  iMovieManager
//
//  Created by HaseeB Arif on 8/9/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import "AddMovie.h"
#define YEAR_LEN 4

@interface AddMovie ()

//private variable.
{ @private BOOL OnSaveCalled; }

@end

@implementation AddMovie
@synthesize delegate;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set Save called to false.
    OnSaveCalled = NO;
    
    //getting rid of keyboard after typing
    self.movieNameTxt.delegate = self;
    self.yearTxt.delegate = self;
    
    //hides the navigation bar button
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//return key for keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //get rid of keyboard.
    [self.movieNameTxt resignFirstResponder];
    [self.yearTxt resignFirstResponder];
    
    return YES;
}

//for Movie Year length restrictions
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(  ( (self.yearTxt.text)  >= ([NSString stringWithFormat:@"%d",YEAR_LEN]) )   && (range.location == YEAR_LEN))
    {
        //Show the Alert for Length Error
        [self showAlert:@"Length Error!" andMessage:@"Movie Year length cannot be greater than 4 digits!"];
        return NO;
    }
    
    else
        return YES;
}



- (IBAction)onSaveButton:(id)sender {
    
    OnSaveCalled = YES;
    
    //get the Name and year from TextField
    self.MovieName = [self.movieNameTxt text];
    self.year = [self.yearTxt text];
    
    if(self.MovieName.length > 0){
        
        [self.movieNameTxt setText:@""];
        [self.yearTxt setText:@""];
        
        //add movie to the MoviesList.
        @try {
             [delegate MovieData:self.MovieName andYear:self.year];
            
             [self showAlert:@"Movie Alert!" andMessage:@"Movie added successfully !"];
            
        } @catch (NSException *ex) {
            NSLog(@"Exception in : addMovie *movieObj = %@",ex);
        }
        
    }
    
    else
    [self showAlert:@"Movie Alert!" andMessage:@"Movie name cannot be empty!"];
}

- (IBAction)onCancelButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)showAlert:(NSString*)title andMessage:(NSString*)message
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    //Handling ok button action.
                                    
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                
                                    //if Save button is called only then dismiss the ViewController.
                                    if(OnSaveCalled && [_MovieName length] > 0)
                                    [self.navigationController popViewControllerAnimated:YES];
                                    
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];

}

@end
