//
//  SortPicker.h
//  iMovieManager
//
//  Created by HaseeB Arif on 8/11/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol mSortProtocol <NSObject>

-(void)sortByPicker:(NSString*)sortBySelected;

@end

@interface SortPicker : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
@property(nonatomic,assign)id delegate;
@end
