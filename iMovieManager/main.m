//
//  main.m
//  iMovieManager
//
//  Created by HaseeB Arif on 8/9/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    
    @autoreleasepool {
    
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
    
    
}
