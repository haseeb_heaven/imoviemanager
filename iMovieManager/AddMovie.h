//
//  AddMovie.h
//  iMovieManager
//
//  Created by HaseeB Arif on 8/9/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoviesList.h"

@protocol MovieDataProtocol <NSObject>

-(void)MovieData:(NSString*)movieName andYear:(NSString*)year;

@end

@interface AddMovie : UIViewController<UITextFieldDelegate>
@property(nonatomic,assign)id delegate;
@property NSString *MovieName;
@property NSString *year;

@property (weak, nonatomic) IBOutlet UITextField *movieNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *yearTxt;


- (IBAction)onSaveButton:(id)sender;
- (IBAction)onCancelButton:(id)sender;

-(void)showAlert:(NSString*)title andMessage:(NSString*)message;

@end

