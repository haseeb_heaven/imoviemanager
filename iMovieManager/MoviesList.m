//
//  MoviesList.m
//  iMovieManager
//
//  Created by HaseeB Arif on 8/9/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import "MoviesList.h"
#import "AddMovie.h"
#import "FetchMovieData.h"
#import "SortPicker.h"

@protocol senddataProtocol;

@interface MoviesList ()

{
@private NSInteger mIndex,sortIndex,yearIndex,runTimeIndex,ratingIndex;
@private BOOL sortedOnce;
@private NSMutableArray *sortSections,*moviesRuntimeList,*moviesRatingList,*moviesYearList,*yearTitleList,
    *ratingTitleList,*runtimeTitleList;
@private NSArray *mSortedYearArray,*mSortedRatingArray,*mSortedRunTimeArray;
@private NSString *sortBy;
@private FetchMovieData *mFetchObj;
@private UIActivityIndicatorView *mSpinner;
@private UILabel* spinnerLabel;
@private BOOL alreadyFetched;

    
@private NSMutableArray *sections,*list;
}

@end

@implementation MoviesList

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    //initialize when the View is Created .
    mFetchObj = [[FetchMovieData alloc] init];
    _moviesNames = [[NSMutableArray alloc] init];
    _moviesYears = [[NSMutableArray alloc] init];
    moviesYearList = [[NSMutableArray alloc] init];
    moviesRatingList = [[NSMutableArray alloc] init];
    moviesRuntimeList = [[NSMutableArray alloc] init];
    yearTitleList = [[NSMutableArray alloc] init];
    ratingTitleList = [[NSMutableArray alloc] init];
    runtimeTitleList = [[NSMutableArray alloc] init];
    sections = [[NSMutableArray alloc] init];
    list = [[NSMutableArray alloc] init];
    sortSections = [NSMutableArray arrayWithObject:@"Movies List"];
   
    sortedOnce = NO;
    alreadyFetched = NO;
    mIndex = 0;
    sortIndex = 0;
    yearIndex = 0;
    runTimeIndex = 0;
    ratingIndex = 0;
    
    
    
    //Setting Spinner.
    mSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    mSpinner.frame = CGRectMake((self.view.frame.size.width)/4.5,(self.view.frame.size.height)/2.9, 20.0, 20.0);
    mSpinner.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    mSpinner.backgroundColor = [UIColor clearColor];
    [self.view addSubview:mSpinner];
    
    CGFloat labelX = mSpinner.bounds.size.width + 90;
    spinnerLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelX,(self.view.frame.size.height)/-7, self.view.bounds.size.width - (labelX + 2), self.view.frame.size.height)];
    spinnerLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    spinnerLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    spinnerLabel.numberOfLines = 1;
    spinnerLabel.backgroundColor = [UIColor clearColor];
    spinnerLabel.textColor = [UIColor blackColor];
    spinnerLabel.text = @"Fetching Data Please Wait...";
    spinnerLabel.hidden = YES;
    [self.view addSubview:spinnerLabel];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)MovieData:(NSString *)movieName andYear:(NSString *)year{
    
    [self.moviesNames insertObject: movieName atIndex:mIndex];
    [self.moviesYears insertObject: year atIndex:mIndex];
    
    mIndex++;
}


//Methods for Table View.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sortSections count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    @try{
        return [sortSections objectAtIndex:section];
    }@catch(NSException *ex){
        NSLog(@"Exception in titleForHeaderInSection :%@",ex);
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    @try {
        return [_moviesNames count];
    } @catch (NSException *ex) {
        NSLog(@"Exception in : numberOfRowsInSection : %@",ex);
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *moviesCell = [tableView dequeueReusableCellWithIdentifier:@"moviesCell" forIndexPath:indexPath];
    
        @try {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //Update on UI thread.
          
                        if(indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3){
                            
                            if([sortBy isEqualToString:@"Year"]){
                                moviesCell.textLabel.text = [NSString stringWithFormat:@"%@", [yearTitleList objectAtIndex:indexPath.row]];
                                moviesCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [moviesYearList objectAtIndex:indexPath.row]];

                            }
                            
                            if([sortBy isEqualToString:@"Rating"]){
                                moviesCell.textLabel.text = [NSString stringWithFormat:@"%@", [ratingTitleList objectAtIndex:indexPath.row]];
                                moviesCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [moviesRatingList objectAtIndex:indexPath.row]];
                            }

                            
                            if([sortBy isEqualToString:@"RunTime"]){
                                moviesCell.textLabel.text = [NSString stringWithFormat:@"%@", [runtimeTitleList objectAtIndex:indexPath.row]];
                                moviesCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [moviesRuntimeList objectAtIndex:indexPath.row]];
                            }

                            
                            if(sortBy == nil || ([_moviesNames count] == indexPath.section) ){
                                moviesCell.textLabel.text = [NSString stringWithFormat:@"%@", [_moviesNames objectAtIndex:indexPath.row]];
                                moviesCell.detailTextLabel.text = nil;
                            }
                            
                        }
                        
                 
                      
                    });
                
            } @catch (NSException *exception) {
                NSLog(@"Exception in moviesCell %@",exception);
            }
        
    return moviesCell;
}
//Methods for Table Views ends here.


//opens the ViewPicker for Sorting.
- (IBAction)sortButton:(id)sender {
    
    if([_moviesNames count] == 0){
        [self showAlert:@"Fetch Error" andMessage:@"Please Fetch the Data first!"];
        return;
    }

    UIStoryboard*  storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SortPicker* mSortObj = [storyBoard instantiateViewControllerWithIdentifier:@"sortPickerID"];
    
    mSortObj.delegate = self;
    
    [self.navigationController pushViewController:mSortObj animated:YES];
    
}

//recieves back the SortOption from SortPicker class.
-(void)sortByPicker:(NSString*)sortBySelected{
    
    sortBy = sortBySelected;
    
    if([sortBySelected isEqual:nil])
        return;
    
    if([sortSections containsObject:sortBySelected]){
        [self showAlert:@"Sort Error" andMessage:@"Section is already Present"];
        return;
    }
    
    else
    {
        
        @try {
            [sortSections insertObject:sortBySelected atIndex:sortIndex++];
        }
        @catch (NSException *exception){
            NSLog(@"Exception in sortByPicker : %@",exception);
        }
        
    }
    
    
    //calls start sorting
    [self startSorting:sortBySelected];
}

-(void)startSorting:(NSString*)sortingOption{
    
    
    //start Sorting the Movies
    if(_moviesNames.count == 0){
        [self showAlert:@"Sort Error" andMessage:@"Cannot sort. Movies list is empty"];
        return;
    }
    
    if([_moviesNames containsObject:@"error"]){
        [self showAlert:@"Sort Error" andMessage:@"Error in sorting"];
        return;
    }
    
    //start sorting.
    else
    {
        if(sortingOption != NULL){
            
            
            
            //sending Movies list one by one to sortMovieData method for sorting.
            for(int i = 0; i < [_moviesNames count]; i++){
                [self sortMovieData:[_moviesNames objectAtIndex:i]
                            andYear:[_moviesYears objectAtIndex:i] sort:sortingOption];
                NSLog(@"moviesNames count = %lu",(unsigned long)[_moviesNames count]);
                }
        }
        
    }
    
}

//sorts Movie data.
-(void)sortMovieData:(NSString*)title andYear:(NSString*)Year sort:(NSString*)sortedBy{
    
    sortBy = sortedBy;

    
    
    @try {
        
        @try {
            
            if([mFetchObj.mYearArray objectAtIndex:0] != NULL && [sortBy isEqualToString:@"Year"]){
                NSLog(@"mFetchObj.mYear  = %@",[mFetchObj.mYearArray objectAtIndex:yearIndex]);
                [moviesYearList insertObject:[mSortedYearArray objectAtIndex:yearIndex] atIndex:yearIndex];
                yearIndex++;
            }
            
            if([mFetchObj.mRatingArray objectAtIndex:0] != NULL && [sortBy isEqualToString:@"Rating"]){
                NSLog(@"mFetchObj.Rating  = %@",[mFetchObj.mRatingArray objectAtIndex:ratingIndex]);
                [moviesRatingList insertObject:[mSortedRatingArray objectAtIndex:ratingIndex] atIndex:ratingIndex];
                ratingIndex++;
            }
            
            if([mFetchObj.mRunTimeArray objectAtIndex:0] != NULL && [sortBy isEqualToString:@"RunTime"]){
                NSLog(@"mFetchObj.RunTime = %@",[mFetchObj.mRunTimeArray objectAtIndex:runTimeIndex]);
                [moviesRuntimeList insertObject:[mSortedRunTimeArray objectAtIndex:runTimeIndex] atIndex:runTimeIndex];
                runTimeIndex++;
            }
            
            
            
        } @catch (NSException *exception) {
            NSLog(@"exception in sortMovieData at insertion : %@",exception.reason);
        }
        
        
    } @catch (NSException *exception) {
        NSLog(@"Exception in sortMovieData %@",exception.reason);
    }
    
}



- (IBAction)addButton:(id)sender {
    UIStoryboard*  storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddMovie* mAddMovie = [storyBoard instantiateViewControllerWithIdentifier:@"addMovieID"];
    
    mAddMovie.delegate = self;
    
    [self.navigationController pushViewController:mAddMovie animated:YES];
}

//fetches all the movie details.
- (IBAction)fetchButton:(id)sender {
    
    if(alreadyFetched){
        [self showAlert:@"Fetch error!" andMessage:@"Data has already been fetched!"];
        return;
    }
    
    if([_moviesNames count] == 0){
        [self showAlert:@"Empty error!" andMessage:@"Error in fetching empty list!"];
        return;
    }
    
    
    //set the Spinner to start loading
    [mSpinner startAnimating];
    spinnerLabel.hidden = NO;
    
    [NSThread detachNewThreadSelector:@selector(fetchMovieData) toTarget:self withObject:nil];
    
}


-(void)fetchMovieData{
    
    //sending Movies list one by one to fetchDataAsync method.
    for(int i = 0; i < [_moviesNames count]; i++)
        [mFetchObj fetchDataJSON:[_moviesNames objectAtIndex:i] andYear:[_moviesYears objectAtIndex:i]];

    if([_moviesNames count] <= 3)
        [NSThread sleepForTimeInterval:3.0f];
    
    if([_moviesNames count] >= 4 && [_moviesNames count] <=7)
        [NSThread sleepForTimeInterval:5.0f];
    
    if([_moviesNames count] >= 8 && [_moviesNames count] <=15)
        [NSThread sleepForTimeInterval:7.0f];
    
    [mSpinner performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
    spinnerLabel.hidden = YES;
    
    NSLog(@"fetch completed");
    
        dispatch_async(dispatch_get_main_queue(), ^{
        // Update on UI thread.
            NSLog(@"alert Called first");
             NSLog(@"mFetchObj.mDictionaryArray count = %lu",(unsigned long)[mFetchObj.mYearDictionaryArray count]);
        [self showAlert:@"Fetch Alert" andMessage:@"Fetching of data completed successfully!"];
            
            
            //sort the Year to descending order.
            NSSortDescriptor *yearSortDesc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
            mSortedYearArray = [mFetchObj.mYearArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:yearSortDesc]];

                            @try {
    
                        //Getting the related Movie Title for Year from NSDictionaryArray.
                        for(int i = 0; i < [_moviesNames count];i++){
                            
                            for(int j = 0; j < [_moviesNames count];j++){
                                
                            NSArray *movieKeyValue = [[mFetchObj.mYearDictionaryArray objectAtIndex:j] valueForKey:[mSortedYearArray objectAtIndex:i]];
                                
                            if(movieKeyValue != nil)
                            
                                    [yearTitleList insertObject:[[mFetchObj.mYearDictionaryArray objectAtIndex:j] valueForKey:[mSortedYearArray objectAtIndex:i]] atIndex:i];
                            }
                            
            
                            NSLog(@"yearTitleList at %d is = %@",i,[yearTitleList objectAtIndex:i]);
                             }
                                
                               } @catch (NSException *exception) {
                                NSLog(@"Exception in yearTitleList = %@",exception);
                                }

            
        //sort the Rating to descending order.
        NSSortDescriptor *ratingSortDesc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
        mSortedRatingArray = [mFetchObj.mRatingArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:ratingSortDesc]];

            @try {
                
                //Getting the related Movie Title for Year from NSDictionaryArray.
                for(int i = 0; i < [_moviesNames count];i++){
                    
                    for(int j = 0; j < [_moviesNames count];j++){
                        
                        NSArray *movieKeyValue = [[mFetchObj.mRatingDictionaryArray objectAtIndex:j] valueForKey:[mSortedRatingArray objectAtIndex:i]];
                        
                        if(movieKeyValue != nil)
                            
                            [ratingTitleList insertObject:[[mFetchObj.mRatingDictionaryArray objectAtIndex:j] valueForKey:[mSortedRatingArray objectAtIndex:i]] atIndex:i];
                    }
                    
                    
                    NSLog(@"ratingTitleList at %d is = %@",i,[ratingTitleList objectAtIndex:i]);
                }
                
            } @catch (NSException *exception) {
                NSLog(@"Exception in ratingTitleList = %@",exception);
            }
            

            //sort the RunTime to descending order.
            NSSortDescriptor *runtimeSortDesc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
            mSortedRunTimeArray = [mFetchObj.mRunTimeArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:runtimeSortDesc]];
            
            @try {
                
                //Getting the related Movie Title for Year from NSDictionaryArray.
                for(int i = 0; i < [_moviesNames count];i++){
                    
                    for(int j = 0; j < [_moviesNames count];j++){
                        
                        NSArray *movieKeyValue = [[mFetchObj.mRunTimeDictionaryArray objectAtIndex:j] valueForKey:[mSortedRunTimeArray objectAtIndex:i]];
                        
                        if(movieKeyValue != nil)
                            
                        [runtimeTitleList insertObject:[[mFetchObj.mRunTimeDictionaryArray objectAtIndex:j] valueForKey:[mSortedRunTimeArray objectAtIndex:i]] atIndex:i];
                    }
                    
                    
                    NSLog(@"runtimeTitleList at %d is = %@",i,[runtimeTitleList objectAtIndex:i]);
                }
                
            } @catch (NSException *exception) {
                NSLog(@"Exception in runtimeTitleList = %@",exception);
            }
            
            
        });

}

-(void)showAlert:(NSString*)title andMessage:(NSString*)message
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDestructive
                                
                                                      handler:^(UIAlertAction * action) {
                                                          //Handling ok button action.
                                                      }];
    
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end