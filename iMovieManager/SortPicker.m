//
//  SortPicker.m
//  iMovieManager
//
//  Created by HaseeB Arif on 8/11/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import "SortPicker.h"

@interface SortPicker ()
{
@private NSArray *sortList;
@private NSString *sortBy;
}

- (IBAction)doneButton:(id)sender;
@end

@implementation SortPicker
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sortList = @[@"Rating",@"Year",@"RunTime"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//method for Picker View .

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return  sortList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return sortList[row];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    sortBy = sortList[row];
}

//Picker View ends here


- (IBAction)doneButton:(id)sender {
    
    [delegate sortByPicker:sortBy];
    [self.navigationController popViewControllerAnimated:YES];

}
@end
