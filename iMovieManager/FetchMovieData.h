//
//  FetchMovieData.h
//  iMovieManager
//
//  Created by HaseeB Arif on 8/10/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FetchMovieData : NSObject

//Method forward declaration for protected class to use.
-(BOOL)fetchDataJSON:(NSString*)title andYear:(NSString*)Year;
-(NSString*)getSecurePosterURL:(NSString*)URL;
-(void)fetchMoviePoster:(NSURL*)posterURL andCell:(UITableViewCell*)targetCell;

//Proctected Variables.
@property NSMutableArray *mRatingArray,*mYearArray,*mRunTimeArray,*mYearDictionaryArray,*mRatingDictionaryArray,*mRunTimeDictionaryArray;
@property NSString *mPosterURL;
@property NSMutableArray *mPosterURL_List;
@property(nonatomic) NSInteger mPosterURLIndex,mMovieIndex;
@property(nonatomic) BOOL isDataFetched;

@end
