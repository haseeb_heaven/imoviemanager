//
//  FetchMovieData.m
//  iMovieManager
//
//  Created by HaseeB Arif on 8/10/16.
//  Copyright © 2016 haseeb_mir. All rights reserved.
//

#import "FetchMovieData.h"

@implementation FetchMovieData

-(BOOL)fetchDataJSON:(NSString*)title andYear:(NSString*)Year
{
    
    //using the Open Movie Database API to fetch movie details.
    NSString *url = @"https://www.omdbapi.com/?t=&y=&plot=short&r=json";
    
    NSMutableString *movie_url = [NSMutableString stringWithString:url];
    _mRatingArray = [[NSMutableArray alloc] init];
    _mYearArray  = [[NSMutableArray alloc] init];
    _mRunTimeArray = [[NSMutableArray alloc] init];
    _mPosterURL_List = [[NSMutableArray alloc] init];
    _mYearDictionaryArray = [[NSMutableArray alloc] init];
    _mRatingDictionaryArray = [[NSMutableArray alloc] init];
    _mRunTimeDictionaryArray = [[NSMutableArray alloc] init];
    _mPosterURLIndex = 0;
    _mMovieIndex = 0;
    
    title = [title stringByReplacingOccurrencesOfString:@" " withString:@"+"]; //replace spaces with '+' in title for URL format
    
    [movie_url insertString:title atIndex:27]; //insert string at specific index.
    
    //if year is empty.
    if(Year.length == 0)
        ;//dont do anything to url
    
    else{ //insert the year into the URL.
        NSRange range = [movie_url rangeOfString:@"&plot"];
        
        if (range.location == NSNotFound)
            NSLog(@"string was not found");
        else
            [movie_url insertString:Year atIndex:range.location];
    }
    
    NSLog(@"url reqes = %@",movie_url);
    
    
    //Fetching of Movie data starts.
    @try {
        
        //Background Thread for JSON.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURLSession *session = [NSURLSession sharedSession];
            [[session dataTaskWithURL:[NSURL URLWithString:movie_url]
                    completionHandler:^(NSData *data,
                                        NSURLResponse *response,
                                        NSError *error) {
                        
                        // handle response
                        if(data != NULL){
                            NSDictionary *MovieData = [ NSJSONSerialization JSONObjectWithData:data options:0 error:&error]; //data in serialized view
                            
                            
                            //update on Main Thread
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                //initializing Movie Arrays from JSON data.
                                [_mRatingArray insertObject:MovieData[@"imdbRating"] atIndex:_mMovieIndex];
                                [_mYearArray insertObject:MovieData[@"Year"] atIndex:_mMovieIndex];
                                [_mRunTimeArray insertObject:MovieData[@"Runtime"] atIndex:_mMovieIndex];
                                
                                
                                //Trim the Runtime Numbers (remove word min) for making sorting easier.
                                
                                NSInteger trimmedRuntime = [[_mRunTimeArray objectAtIndex:_mMovieIndex ] integerValue];
                                NSLog(@"trimmedRuntime = %ld",(long)trimmedRuntime);
                                
                                [_mRunTimeArray replaceObjectAtIndex:_mMovieIndex withObject:[NSString stringWithFormat:@"%ld",(long)trimmedRuntime]];
                                
                                
                                //_mPosterURL = MovieData[@"Poster"];
                                //[_mPosterURL_List insertObject:_mPosterURL atIndex:_mPosterURLIndex];
                                
                                //Printing Array Logs.
                                NSLog(@"Movie Title = %@",MovieData[@"Title"]);
                                NSLog(@"_mRatingArray = %@",[_mRatingArray objectAtIndex:_mMovieIndex]);
                                NSLog(@"_mYearArray = %@",[_mYearArray objectAtIndex:_mMovieIndex]);
                                NSLog(@"_mRunTimeArray = %@",[_mRunTimeArray objectAtIndex:_mMovieIndex]);
                                
                                //NSLog(@"_mPosterURL = %@",_mPosterURL);
                                
                                
                                
                                //Adding year and title to the dictionary for later use.
                                NSDictionary *yearDictionary = @{
                                            MovieData[@"Year"] : MovieData[@"Title"]};
                                
                                //Adding Dictionary to NSArray to bundle all data together.
                                [_mYearDictionaryArray insertObject:yearDictionary atIndex:_mMovieIndex];
                                
                                
                                NSLog(@"_mYearDictionaryArray = %@",[_mYearDictionaryArray objectAtIndex:_mMovieIndex]);
                                
                                
                                //Adding Rating and title to the dictionary for later use.
                                NSDictionary *ratingDictionary = @{
                                            MovieData[@"imdbRating"] : MovieData[@"Title"]};
                                
                                //Adding Dictionary to NSArray to bundle all data together.
                                [_mRatingDictionaryArray insertObject:ratingDictionary atIndex:_mMovieIndex];
                                
                                NSLog(@"_mRatingDictionaryArray = %@",[_mRatingDictionaryArray objectAtIndex:_mMovieIndex]);
                                
                                
                                
                                
                                
                                //Adding Runtime and title to the dictionary for later use.
                                NSDictionary *runtimeDictionary = @{
                                        [NSString stringWithFormat:@"%ld",(long)trimmedRuntime]: MovieData[@"Title"]};
                                
                                //Adding Dictionary to NSArray to bundle all data together.
                                [_mRunTimeDictionaryArray insertObject:runtimeDictionary atIndex:_mMovieIndex];
                                
                                //xxxx [_mRunTimeDictionaryArray replaceObjectAtIndex:_mMovieIndex withObject:[NSString stringWithFormat:@"%ld",(long)trimmedRuntime]];
                                 
                                NSLog(@"_mRunTimeDictionaryArray = %@",[_mRunTimeDictionaryArray objectAtIndex:_mMovieIndex]);
                                
                                
                                
                                //NSLog(@"Movie Data all : %@",[MovieData description]);
                                
                                _mMovieIndex++;
                                _mPosterURLIndex++;
                                
                            });
                            
                            
                            _isDataFetched = YES;
                        }
                        
                        else{
                            _isDataFetched = NO;
                            NSLog(@"JSON Error = %@",error);
                        }
                        
                    }] resume];
        });
        
        
    } @catch (NSException *exception) {
        NSLog(@"Exception in JSON : %@",exception.reason);
    }
    
    NSLog(@"return _isDataFetched = %@",_isDataFetched ? @"YES" : @"NO");
    return _isDataFetched;
}

-(void)fetchMoviePoster:(NSURL*)posterURL andCell:(UITableViewCell*)targetCell{
    
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:posterURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [targetCell.imageView setImage:image];
                    targetCell.imageView.contentMode = UIViewContentModeScaleAspectFit;
                    [targetCell setNeedsLayout];
                    
                });
            }
        }
    }];
    [task resume];
}

-(NSString*)getSecurePosterURL:(NSString*)URL{
    
    //This is posterURL Demo : http://ia.media-imdb.com/images/
    
    NSMutableString *posterURL = [NSMutableString stringWithString:URL];
    
    [posterURL insertString:@"s" atIndex:4]; //insert string at specific index.
    
    return posterURL;
}


@end
