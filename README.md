# iMovieManager
 _i_**MovieManager** is _i_**Phone** application from which user can sort his/her favorite movies by genre,year etc which helps in sorting movies efficiently.
 it fetches all the movies data from **OMDB** **O**_nline_ **M**_ovie_ **D**_ata-_ **B**_ase_ and provides list of movies 
 from which user can sort his/her favorite movies .


## Application Info.
NAME : _i_**MovieManager**</br>
LANGUAGES USED : **OBJECTIVE-C/HTML**</br>
FRAMEWORK : **COCOA TOUCH BY APPLE**</br>
APPLICATION TYPE : **iPhone APPLICATION**</br>

**_Written by Ha5eeB MiR (haseebmir.hm@gmail.com)_**
